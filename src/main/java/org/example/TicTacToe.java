package org.example;

import java.util.Scanner;

public class TicTacToe {

    private char[][] board;
    private char currentPlayer;

    public TicTacToe() {
        board = new char[3][3];
        currentPlayer = 'X';
        initializeBoard();
    }

    // 初始化棋盘
    public void initializeBoard() {
    }

    // 打印棋盘
    public void printBoard() {
        System.out.println("2024 软件工程 期末考试");
    }

    // 判断棋盘是否满了
    public boolean isBoardFull() {
        return false;
    }

    // 更改玩家
    public void changePlayer() {
    }

    // 放置标记
    public boolean placeMark(int row, int col) {
        return false;
    }

    // 判断是否有玩家获胜
    public boolean checkForWin() {
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TicTacToe game = new TicTacToe();
        game.printBoard();

        while (true) {
            int row, col;
            System.out.println("玩家 " + game.currentPlayer + "，请选择行（1、2、3）: ");
            row = scanner.nextInt() - 1;
            System.out.println("玩家 " + game.currentPlayer + "，请选择列（1、2、3）: ");
            col = scanner.nextInt() - 1;

            if (game.placeMark(row, col)) {
                game.printBoard();
                if (game.checkForWin()) {
                    System.out.println("玩家 " + game.currentPlayer + " 获胜！");
                    break;
                }
                if (game.isBoardFull()) {
                    System.out.println("游戏平局！");
                    break;
                }
                game.changePlayer();
            } else {
                System.out.println("输入的值不合法");
            }
        }
        scanner.close();
    }
}